package org.elastos.checkdid;

import android.app.Activity;

/**
 * transmit AuthReceiver data
 */
class Transmit {

    private static Transmit mInstance;

    private String mPublickey;

    private String mSignature;

    private String mDidName;

    private Transmit(){}


    public static Transmit getInstance(){
        if(mInstance == null){
            mInstance = new Transmit();
        }
        return mInstance;
    }

    public void setPublickey(String publickey){
        this.mPublickey = publickey;
    }

    public String getPublickey(){
        return mPublickey;
    }

    public void setSignature(String publickey){
        this.mSignature = publickey;
    }

    public String getSignature(){
        return mSignature;
    }

    public void setDidName(String publickey){
        this.mDidName = publickey;
    }

    public String getDidName(){
        return mDidName;
    }

}
