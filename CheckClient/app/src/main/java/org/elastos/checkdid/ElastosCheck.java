package org.elastos.checkdid;

import android.app.Activity;
import android.content.Context;

public class ElastosCheck {

    private static Transmit mTransmit;

    public static void init(Activity activity){
        if(activity == null) new Throwable("activity can not null");
        Helper.setActivity(activity);
        mTransmit = Transmit.getInstance();
    }

    public static String getPublickey(){
        if(mTransmit != null){
            return mTransmit.getPublickey();
        }
        return null;
    }

    public static String getDidName(){
        if(mTransmit != null){
            return mTransmit.getDidName();
        }
        return null;
    }

    public static String getSignature(){
        if(mTransmit != null){
            return mTransmit.getSignature();
        }
        return null;
    }

    public static boolean CheckDID(String publicKey, String didName){
        if(publicKey==null||publicKey.isEmpty() || didName==null||didName.isEmpty()) new Throwable("params is invalide");
        return DIDInspector.CheckDID(publicKey, didName);
    }

    public static boolean CheckSign(String publickey,String message, String signature) {
        if(publickey==null||publickey.isEmpty() ||
                message==null||message.isEmpty() ||
                signature==null||signature.isEmpty()) new Throwable("params is invalide");
        return DIDInspector.CheckSign(publickey, message, signature);
    }

    public static void requestAuthor(Context context, String didName, String message){
        if(context==null||
                message==null||message.isEmpty() ||
                message==null||message.isEmpty()) new Throwable("params is invalide");
        Helper.broadDidInfo(context, didName, message);
    }
}
