package org.elastos.checkdid;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

class Helper {

    public static final String CLIENT_DIDNAME_FLAG = "didName";

    public static final String CLIENT_MESSAGE_FLAG = "message";

    public static final String SERVER_RECEIVER_ACTION_FLAG = "com.elastos.server.receiver";

    private static Activity mActivity;

    public static void setActivity(Activity activity){
        mActivity = activity;
    }

    public static void broadDidInfo(Context context, String didName, String message){
        if(context == null) return;
        if(didName==null || didName.isEmpty()) return;
        if(message==null || message.isEmpty()) return;
        Intent intent = new Intent();
        intent.setAction(SERVER_RECEIVER_ACTION_FLAG);
        intent.putExtra(CLIENT_DIDNAME_FLAG, didName);
        intent.putExtra(CLIENT_MESSAGE_FLAG, message);
        context.sendBroadcast(intent);
    }

    public static void back2Forground(){
        if(mActivity == null) new Throwable("mActivity is null");
        ActivityManager activityManager = (ActivityManager) mActivity.getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        int taskId = mActivity.getTaskId();
        activityManager.moveTaskToFront(taskId,  ActivityManager.MOVE_TASK_WITH_HOME);
    }

}
