package org.elastos.checkdid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AuthReceiver extends BroadcastReceiver {

    private Transmit mTransmit;

    private static final String PUBLIC_KEY_FLAG = "publicKey";

    private static final String SIGNATURE_FLAG = "signature";

    private static final String DID_NAME_FLAG = "didName";

    @Override
    public void onReceive(Context context, Intent intent) {
        mTransmit = Transmit.getInstance();
        String publicKey = intent.getStringExtra(PUBLIC_KEY_FLAG);
        String signature = intent.getStringExtra(SIGNATURE_FLAG);
        String didName = intent.getStringExtra(DID_NAME_FLAG);

        mTransmit.setPublickey(publicKey);
        mTransmit.setSignature(signature);
        mTransmit.setDidName(didName);

        Helper.back2Forground();
    }
}
