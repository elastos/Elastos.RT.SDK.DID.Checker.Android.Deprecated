package com.elastos.author.server;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import org.elastos.elastoswallet.ElastosWalletUtils;
import org.elastos.elastoswallet.IDid;
import org.elastos.elastoswallet.IDidManager;
import org.elastos.elastoswallet.IMasterWallet;
import org.elastos.elastoswallet.IdManagerFactory;
import org.elastos.elastoswallet.MasterWalletManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements MainControl.MainCallBack {

  private ListView mWalletList;
  private DidInfoAdapter mAdapter;
  private List<DIDInfo> mData = new ArrayList<>();
  MainControl control = MainControl.getInstance();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mWalletList = findViewById(R.id.wallet_list);
    mAdapter = new DidInfoAdapter(this, mData);
    mWalletList.setAdapter(mAdapter);
    control.setListener(this);

    FloatingActionButton fab = findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
          showNormalDialog(dialogMessage);
      }
    });
  }


  public void createDid(View view) {
    try {
      //1.初始化钱包所需数据
      ElastosWalletUtils.InitConfig(this);

      //2.生成MasterWalletManager
      String rootPath = getApplicationContext().getFilesDir().getParent();
      File file = new File(rootPath);
      if (!file.exists()) {
        file.mkdirs();
      }
      MasterWalletManager masterWalletManager = new MasterWalletManager(rootPath);

      //3.生成IMasterWallet
      String language = "english";
      String mnemonic = masterWalletManager.GenerateMnemonic(language);
      String masterWalletId = "masterWalletId";
      String phrasePassword = "masterWalletId";
      String payPassword = "masterWalletId";
      IMasterWallet iMasterWallet = masterWalletManager.CreateMasterWallet(masterWalletId, mnemonic, phrasePassword, payPassword, language);

      //4.生成IDidManager
      IDidManager iDidManager = IdManagerFactory.CreateIdManager(iMasterWallet, rootPath);
      IDid iDid = iDidManager.CreateDID("masterWalletId");

      //5.获取didName,publicKey,message sign
      String didName = iDid.GetDIDName();
      String publicKey = iDid.GetPublicKey();

      DIDInfo dIdInfo = new DIDInfo(didName, publicKey);

      mData.add(dIdInfo);
      mAdapter.notifyDataSetChanged();

      control.setContext(this);
      control.setActivity(this);
      control.setDIDInfo(mData);
      control.setIDid(iDid);
      control.setPassword(phrasePassword);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private String dialogMessage;

  @Override
  public void showText(String message, String didName) {
    control.back2Forground();
    dialogMessage = "message:"+message+"\n"+"didName:"+didName;
    showNormalDialog(dialogMessage);
  }


  private void showNormalDialog(String message){
      if(message==null || message.isEmpty()) return;
    final AlertDialog.Builder normalDialog =
            new AlertDialog.Builder(MainActivity.this);
    normalDialog.setTitle("client request");
    normalDialog.setMessage(message);
    normalDialog.setPositiveButton("allow",
            new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                control.recallClient();
                dialog.cancel();
              }
            });
    normalDialog.setNegativeButton("forbid",
            new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
            });
    normalDialog.show();
  }

}
