package com.elastos.author.server;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class DidListActivity extends Activity{

    private ListView mListview;
    private DidInfoAdapter mAdapter;
    private List<DIDInfo> mData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_did_list);
        initRecyclerView();
        initListener();
    }

    private void initRecyclerView(){
        mListview = findViewById(R.id.did_list_recycler);
        mAdapter = new DidInfoAdapter(this, mData);
        mListview.setAdapter(mAdapter);
    }

    private void initListener(){
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
    }

    private void getDidInfo(){

    }
}
