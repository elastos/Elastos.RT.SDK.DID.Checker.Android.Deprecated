package com.elastos.author.server;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class DidInfoAdapter extends BaseAdapter {

    private Context mContext;
    private List<DIDInfo> mData;

    public DidInfoAdapter(Context context, List<DIDInfo> data) {
        this.mContext = context;
        this.mData = data;
    }

    public int getCount() {
        return this.mData == null ? 0 : this.mData.size();
    }

    public Object getItem(int position) {
        return this.mData == null ? 0 : this.mData.size();
    }

    public long getItemId(int position) {
        return (long)position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(this.mContext);
            convertView = inflater.inflate(R.layout.did_item_layout, parent, false);
            holder = new ViewHolder();
            holder.mDidName = convertView.findViewById(R.id.did_item_name_edt);
            holder.mPublicKey = convertView.findViewById(R.id.did_item_key_tv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        DIDInfo dIdInfo = this.mData.get(position);
        holder.mDidName.setText(dIdInfo.didName);
        holder.mPublicKey.setText("publicKey:" + dIdInfo.publicKey);
        return convertView;
    }

    static class ViewHolder {
        public EditText mDidName;
        public TextView mPublicKey;

        ViewHolder() {
        }
    }
}
