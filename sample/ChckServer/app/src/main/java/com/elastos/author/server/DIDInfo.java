package com.elastos.author.server;

public class DIDInfo {
    public String didName;
    public String publicKey;

    public DIDInfo(String name, String publicKey) {
        this.didName = name;
        this.publicKey = publicKey;
    }
}
