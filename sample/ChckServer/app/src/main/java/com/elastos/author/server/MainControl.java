package com.elastos.author.server;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;


import org.elastos.elastoswallet.IDid;

import java.util.List;

public class MainControl {
    private static final String CLIENT_RECEIVER_FLAG = "com.elastos.client.receiver";

    private static final String PUBLIC_KEY_FLAG = "publicKey";

    private static final String SIGNATURE_FLAG = "signature";

    private static final String DID_NAME_FLAG = "didName";

    private Context mContext;
    private Activity mActivity;
    private List<DIDInfo> mData;
    private IDid iDid;
    private String password;
    private String message;
    private String didName;
    private MainCallBack listener;
    private static MainControl mInstance;

    private MainControl(){}

    public static MainControl getInstance(){
        if(mInstance == null){
            mInstance = new MainControl();
        }
        return mInstance;
    }

    public void setContext(Context context){
        this.mContext = context;
    }

    public void setActivity(Activity activity){
        this.mActivity = activity;
    }

    public void setDIDInfo(List<DIDInfo> data){
        this.mData = data;
    }

    public void setIDid(IDid iDid){
        this.iDid = iDid;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setDidName(String didName){
        this.didName = didName;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public void setListener(MainCallBack listener){
        this.listener = listener;
    }

    public void showText(String message, String didName){
        if(listener != null){
            listener.showText(message, didName);
        }
    }

    public void recallClient() {
        if (mContext == null) return;
        if(didName==null || didName.isEmpty()) return;
        if (mData==null || mData.size()==0) return;

        String signature = "";
        try{
            signature = iDid.Sign(message, password);
        } catch (Exception e) {

        }
        for(DIDInfo didInfo : mData){
            if(didInfo.didName.equals(didName)){
                Intent intent = new Intent();
                intent.setAction(CLIENT_RECEIVER_FLAG);
                intent.putExtra(PUBLIC_KEY_FLAG, didInfo.publicKey);
                intent.putExtra(SIGNATURE_FLAG, signature);
                intent.putExtra(DID_NAME_FLAG, didInfo.didName);
                mContext.sendBroadcast(intent);
            }
        }
    }

    public void back2Forground(){
        if(mActivity == null) return;
        ActivityManager activityManager = (ActivityManager) mActivity.getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        int taskId = mActivity.getTaskId();
        activityManager.moveTaskToFront(taskId,  ActivityManager.MOVE_TASK_WITH_HOME);
    }

    interface MainCallBack {
        void showText(String message, String didName);
    }
}
