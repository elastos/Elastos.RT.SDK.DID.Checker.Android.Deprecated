package com.elastos.author.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServerReceiver extends BroadcastReceiver {
    MainControl control = MainControl.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        String didName = intent.getStringExtra("didName");
        String message = intent.getStringExtra("message");
        control.setMessage(message);
        control.setDidName(didName);

        control.showText(message, didName);

    }
}
