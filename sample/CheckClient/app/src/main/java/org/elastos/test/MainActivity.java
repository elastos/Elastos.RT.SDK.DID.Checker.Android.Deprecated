package org.elastos.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.elastos.checkdid.ElastosCheck;
import org.elastos.checkdid.test.R;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private EditText mDidEdt;
    private EditText mMessageEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDidEdt = findViewById(R.id.did_name_edt);
        mMessageEdt = findViewById(R.id.did_message_edt);
        ElastosCheck.init(this);
    }

    public void check(View view) {
        String message = mMessageEdt.getText().toString();

        String publicKey = ElastosCheck.getPublickey();
        String didName = ElastosCheck.getDidName();
        String signature = ElastosCheck.getSignature();

        Boolean checkDid =  ElastosCheck.CheckDID(publicKey, didName);
        Boolean checkSign = ElastosCheck.CheckSign(publicKey, message, signature);
        if(checkDid && checkSign) Log.i(TAG, "login success!!");

        showNormalDialog("checkDid:"+checkDid+"\n"+"checkSign:"+checkSign);
    }

    public void getDidInfo(View view){
        String didName = mDidEdt.getText().toString();
        String message = mMessageEdt.getText().toString();
        if(didName!=null && !didName.isEmpty()){
            ElastosCheck.requestAuthor(this, didName, message);
        }

    }


    private void showNormalDialog(String message){
        if(message==null || message.isEmpty()) return;
        final AlertDialog.Builder normalDialog =
                new AlertDialog.Builder(MainActivity.this);
        normalDialog.setTitle("check result");
        normalDialog.setMessage(message);
        normalDialog.setPositiveButton("Know it",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        normalDialog.show();
    }
}
