# Elastos.RT.SDK.DID.Checker.Android
## 1.download aar package
 https://github.com/elastos/Elastos.RT.SDK.DID.Checker.Android/releases

## 2.import aar to your project
 Using Android Studio,File -> New -> New Module -> Import .jar/.aar and import your aar  
 Then in your project’s build.gradle (the one under ‘app’) add the following:  

> dependencies {

>>  implementation project(':checkId')

> }

## 3.API
### Goto your entry Activity（android.intent.action.MAIN） and insert the following code:
 ElastosCheck.init(activity);  

### request authorization
 ElastosCheck.requestAuthor(context, didName, message);  

### get authorize info
 ElastosCheck.getPublickey();  
 ElastosCheck.getSignature();  
 ElastosCheck.getDidName();  

### check authorize info
 ElastosCheck.CheckDID(publicKey, didName);  
 ElastosCheck.CheckSign(publicKey, message, signature);  
